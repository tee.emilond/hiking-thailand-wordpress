<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hikingthai
 */

?>

            
			<!--  Footer. Class fixed for fixed footer  -->
			<footer class="fixed full-width">
					<div class="container">
							<div class="row no-margin">
									<div class="col-sm-4 col-md-2 padding-leftright-null">
											<h6 class="heading white margin-bottom-extrasmall">Dolomia</h6>
											<ul class="sitemap">
													<li><a href="">Home</a></li>
													<li><a href="">Pages</a></li>
													<li><a href="">Portfolio</a></li>
													<li><a href="">Blog</a></li>
													<li><a href="">Elements</a></li>
													<li><a href="">Contacts</a></li>
											</ul>
									</div>
									<div class="col-sm-4 col-md-2 padding-leftright-null">
											<h6 class="heading white margin-bottom-extrasmall">Useful Links</h6>
											<ul class="useful-links">
													<li><a href="">FAQs</a></li>
													<li><a href="">Download Catalog</a></li>
													<li><a href="">Privacy Policy</a></li>
													<li><a href="">Cookie Policy</a></li>
											</ul>
									</div>
									<div class="col-sm-4 col-md-4 padding-leftright-null">
											<h6 class="heading white margin-bottom-extrasmall">Contact Us</h6>
											<ul class="info">
													<li>Phone <a href="">+39 123456789</a></li>
													<li>Mail <a href="">mail@domain.com</a></li>
													<li>Monday to Friday <span class="white">9.00 am to 8.00 pm</span><br>Saturday from <span class="white">9.00 am to 12.00 pm</span></li>
													<li><a href="">322 Moon St, Venice<br>
															Italy, 1231</a></li>
											</ul>
									</div>
									<div class="col-md-4 padding-leftright-null">
											<h6 class="heading white margin-bottom-extrasmall">Passionate About Nature</h6>
											<p class="grey-light">Sign up for Dolomia mounthly newsletter and get to know more about our latest adventures and much mores...</p>
											<div id="newsletter-form" class="padding-onlytop-xs">
													<form class="search-form">
															<div class="form-input">
																	<input type="text" placeholder="Your email ID">
																	<span class="form-button">
																			<button type="button">Sign Up</button>
																	</span>
															</div>
													</form>
											</div>
									</div>
							</div>
							<div class="copy">
									<div class="row no-margin">
											<div class="col-md-8 padding-leftright-null">
													&copy; 2016 Dolomia -  Hiking &amp; Outdoor Html Template Handmade by <a href="http://www.patrickdavid.it">puredesignThemes</a>
											</div>
											<div class="col-md-4 padding-leftright-null">
													<ul class="social">
															<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
															<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
															<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
															<li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
															<li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
													</ul>
											</div>
									</div>
							</div>
					</div>
			</footer>
			<!--  END Footer. Class fixed for fixed footer  -->
	</div>
	<!--  Main Wrap  -->
	
	<?php wp_footer(); ?> 
    </body>
</html>