<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hikingthai
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <!-- <title><?php bloginfo('name')?></title> -->
				<?php wp_head(); ?>
        
    </head>
    <body <?php body_class(); ?>>
      
        <!--  loader  -->
        <div id="myloader">
            <span class="loader">
                <div class="inner-loader"></div>
            </span>
        </div>
        
        <!--  Main Wrap  -->
        <div id="main-wrap" class="full-width">
            <!--  Header & Menu  -->
            <header id="header" class="fixed transparent full-width">
                <div class="container">
                    <nav class="navbar navbar-default white">
                        <!--  Header Logo  -->
                        <div id="logo">
                            <a class="navbar-brand" href="index.html">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo.png" class="normal" alt="logo">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo@2x.png" class="retina" alt="logo">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo_white.png" class="normal white-logo" alt="logo">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo_white@2x.png" class="retina white-logo" alt="logo">
                            </a>
                        </div>
                        <!--  END Header Logo  -->
                        <!--  Classic menu, responsive menu classic  -->
                        <div id="menu-classic">
                            <div class="menu-holder">
                                <ul>
                                    <li class="submenu">
                                        <a href="javascript:void(0)" class="active-item">Home</a>
                                        <ul class="sub-menu">
                                            <li><a href="index.html">Home Classic</a></li>
                                            <li><a href="index-carousel.html">Home Carousel</a></li>
                                            <li><a href="index-showcase.html">Home Showcase</a></li>
                                        </ul>
                                    </li>
                                    <li class="submenu">
                                        <a href="javascript:void(0)">Pages</a>
                                        <ul class="sub-menu">
                                            <li><a href="about-me.html">About Me</a></li>
                                            <li><a href="about-us.html">About Us</a></li>
                                            <li><a href="single-trek.html">Single Trek 1</a></li>
                                            <li><a href="single-trek-2.html">Single Trek 2</a></li>
                                            <li><a href="404.html">404</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="treks.html">Treks</a>
                                    </li>
                                    <li class="submenu">
                                        <a href="javascript:void(0)">Gallery</a>
                                        <ul class="sub-menu">
                                            <li><a href="gallery.html">Classic</a></li>
                                            <li><a href="gallery-filters.html">Filters</a></li>
                                            <li><a href="single-project-1.html">Single Project 1</a></li>
                                            <li><a href="single-project-2.html">Single Project 2</a></li>
                                        </ul>
                                    </li>
                                    <li class="submenu">
                                        <a href="javascript:void(0)">Blog</a>
                                        <ul class="sub-menu">
                                            <li><a href="blog-classic.html">Blog Classic</a></li>
                                            <li><a href="standard-post.html">Image Post</a></li>
                                            <li><a href="slider-post.html">Slider Post </a></li>
                                            <li><a href="video-post.html">Video Post</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="elements.html">Elements</a>
                                    </li>
                                    <li>
                                        <a href="contact-1.html">Contact</a>
                                    </li>
                                    <!-- Search Icon -->
                                    <li class="search">
                                        <i class="icon ion-ios-search"></i>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--  END Classic menu, responsive menu classic  -->
                        <!--  Button for Responsive Menu Classic  -->
                        <div id="menu-responsive-classic">
                            <div class="menu-button">
                                <span class="bar bar-1"></span>
                                <span class="bar bar-2"></span>
                                <span class="bar bar-3"></span>
                            </div>
                        </div>
                        <!--  END Button for Responsive Menu Classic  -->
                        <!--  Search Box  -->
                        <div id="search-box" class="full-width">
                            <form role="search" id="search-form" class="black big">
                                <div class="form-input">
                                    <input class="form-field black big" type="search" placeholder="Search...">
                                    <span class="form-button big">
                                        <button type="button">
                                            <i class="icon ion-ios-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </form>
                            <button class="close-search-box">
                                <i class="icon ion-ios-close-empty"></i>
                            </button>
                        </div>
                        <!--  END Search Box  -->
                    </nav>
                </div>
            </header>
            <!--  END Header & Menu  -->
