<?php
/**
 * The main template file for front-page
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hikingthai
 */

get_header();
?>
<!--  Page Content, class footer-fixed if footer is fixed  -->
<div id="page-content" class="header-static footer-fixed">
    <!--  Slider  -->
    <?php if( have_rows('slide_images') ): ?>
    <div id="flexslider-nav" class="fullpage-wrap small">
        <ul class="slides">
        <?php while( have_rows('slide_images') ): the_row(); 
    
        ?>

            <li style="background-image:url(<?php the_sub_field('background'); ?>)">
                <div class="container text">
                    <h1 class="white flex-animation"><?php the_sub_field('title'); ?></h1>
                    <?php if(get_sub_field('excerpt')): ?>
                    <h2 class="white flex-animation"><?php the_sub_field('excerpt'); ?></h2>
                    <?php endif; ?>
                    <?php if(get_sub_field('button_title')): ?>
                    <a href="<?php the_sub_field('button_url'); ?>" class="shadow btn-alt small activetwo margin-bottom-null flex-animation"><?php the_sub_field('button_title'); ?></a>
                    <?php endif; ?>
                </div>
                <div class="gradient dark"></div>
            </li>
        <?php endwhile; ?>
        </ul>
        
        <div class="slider-navigation">
            <a href="#" class="flex-prev"><i class="icon ion-ios-arrow-thin-left"></i></a>
            <div class="slider-controls-container"></div>
            <a href="#" class="flex-next"><i class="icon ion-ios-arrow-thin-right"></i></a>
        </div>
    </div>
    <?php endif; ?>

    <!--  END Slider  -->
    <div id="home-wrap" class="content-section fullpage-wrap">
        <!-- Section About -->
        <div class="row margin-leftright-null">
            <div class="container">
                <div class="col-md-12 padding-leftright-null">
                    <div class="text text-center">
                        <h2 class="margin-bottom-null title center"><?php the_field('about_title'); ?></h2>
                        <?php if(get_field('about_excerpt')): ?>
                        <p class="heading center grey z-index"><?php the_field('about_excerpt'); ?></p>
                        <?php endif; ?>
                        <?php if(get_field('about_detail')): ?>
                        <div class="padding-onlytop-md">
                          <div><?php the_field('about_detail'); ?></div> 
                            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic reprehenderit eligendi dolor incidunt natus commodi doloribus culpa ad iusto suscipit dolores, ex obcaecati molestias, nulla consectetur dolore quas adipisci similique eaque provident. Nostrum, officiis cum facere iure et temporibus reprehenderit, vero laudantium numquam qui odit. Dolor et inventore earum, deleniti!</p>                                -->
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Section About -->
        <!-- Services -->
        <div class="row no-margin text-center grey-background">
            <div class="container">
                <div class="col-md-12 padding-leftright-null">
                    <div class="col-md-4 padding-leftright-null">
                        <div class="text padding-md-bottom-null">
                            <i class="pd-icon-cloudy-day service margin-bottom-null"></i>
                            <h6 class="heading margin-bottom-extrasmall">In every conditions</h6>
                            <p class="margin-bottom-null">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem harum aspernatur sapiente error, voluptas fuga, laudantium ullam magni fugit. Qui!</p>
                        </div>
                    </div>
                    <div class="col-md-4 padding-leftright-null">
                        <div class="text padding-md-bottom-null">
                            <i class="pd-icon-camp-bag service margin-bottom-null"></i>
                            <h6 class="heading margin-bottom-extrasmall">Professional Team</h6>
                            <p class="margin-bottom-null">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem harum aspernatur sapiente error, voluptas fuga, laudantium ullam magni fugit. Qui!</p>
                        </div>
                    </div>
                    <div class="col-md-4 padding-leftright-null">
                        <div class="text">
                            <i class="pd-icon-camping-knief service margin-bottom-null"></i>
                            <h6 class="heading  margin-bottom-extrasmall">Expert hikers</h6>
                            <p class="margin-bottom-null">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem harum aspernatur sapiente error, voluptas fuga, laudantium ullam magni fugit. Qui!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Services -->
        <!-- Carousel Gallery -->
        <div class="row margin-leftright-null padding-sm">
            <div class="gallery-carousel">
                <div class="item">
                    <div class="image" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/img/trip5-small.jpg)"></div>
                </div>
                <div class="item">
                    <div class="image" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/img/trip6-small.jpg)"></div>
                </div>
                <div class="item">
                    <div class="image" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/img/trip7-small.jpg)"></div>
                </div>
                <div class="item">
                    <div class="image" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/img/trip8-small.jpg)"></div>
                </div>
                <div class="item">
                    <div class="image" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/img/trip9-small.jpg)"></div>
                </div>
                <div class="item">
                    <div class="image" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/img/trip10-small.jpg)"></div>
                </div>
                <div class="item">
                    <div class="image" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/img/trip11-small.jpg)"></div>
                </div>
                <div class="item">
                    <div class="image" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/img/trip12-small.jpg)"></div>
                </div>
                <div class="item">
                    <div class="image" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/img/trip2-small.jpg)"></div>
                </div>
                <div class="item">
                    <div class="image" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/img/trip8-small.jpg)"></div>
                </div>
                <div class="item">
                    <div class="image" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/img/trip9-small.jpg)"></div>
                </div>
            </div>
        </div>
        <!-- Carousel Gallery -->
        <!-- Trip Showcase  -->
        <div id="showcase-treks" class="text padding-bottom-null grey-background center">
            <div class="container">
                <div class="col-md-12 padding-leftright-null text-center">
                    <h2 class="margin-bottom-null title line center">Featured trips</h2>
                    <p class="heading center grey margin-bottom-null">Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                </div>
                <div class="col-md-12 padding-leftright-null">
                    <section class="showcase-carousel text">
                        <!--  Single Trip  -->
                        <div class="item">
                            <div class="showcase-trek">
                                <span class="read">
                                    from 820$
                                </span>
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/trip1.jpg" alt="">
                                <div class="content text-center">
                                    <div class="row margin-leftright-null">
                                        <div class="meta">
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3>12</h3>
                                                <h4>Days</h4>
                                            </div>
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3>10</h3>
                                                <h4>Max Group Size</h4>
                                            </div>
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3>Hight</h3>
                                                <h4>Difficulty</h4>
                                            </div>
                                        </div>
                                        <div class="category">
                                            <h3>Trekking Expedition</h3>
                                        </div>
                                        <div class="info">
                                            <div class="col-md-12 padding-leftright-null">
                                                <h6 class="heading">Everest Base Camp</h6>
                                                <p class="margin-null">Ama Dablam, Nepal</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="single-trek.html" class="link"></a>
                            </div>
                        </div>
                        <!--  END Single Trip  -->
                        <div class="item">
                            <div class="showcase-trek">
                                <span class="read">
                                    from 320$
                                </span>
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/trip2.jpg" alt="">
                                <div class="content text-center">
                                    <div class="row margin-leftright-null">
                                        <div class="meta">
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3>3</h3>
                                                <h4>Days</h4>
                                            </div>
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3>10</h3>
                                                <h4>Max Group Size</h4>
                                            </div>
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3>Hight</h3>
                                                <h4>Difficulty</h4>
                                            </div>
                                        </div>
                                        <div class="category">
                                            <h3>Hiking Expedition</h3>
                                        </div>
                                        <div class="info">
                                            <div class="col-md-12 padding-leftright-null">
                                                <h6 class="heading">Monte Bianco</h6>
                                                <p class="margin-null">Courmayeur, France</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="single-trek-2.html" class="link"></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="showcase-trek">
                                <span class="read">
                                    from 120$
                                </span>
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/trip3.jpg" alt="">
                                <div class="content text-center">
                                    <div class="row margin-leftright-null">
                                        <div class="meta">
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3>1</h3>
                                                <h4>Days</h4>
                                            </div>
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3>20</h3>
                                                <h4>Max Group Size</h4>
                                            </div>
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3>Medium</h3>
                                                <h4>Difficulty</h4>
                                            </div>
                                        </div>
                                        <div class="category">
                                            <h3>Canyoning Expedition</h3>
                                        </div>
                                        <div class="info">
                                            <div class="col-md-12 padding-leftright-null">
                                                <h6 class="heading">Valley of Piave</h6>
                                                <p class="margin-null">Dolomiti, Italy</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="single-trek.html" class="link"></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="showcase-trek">
                                <span class="read">
                                    from 550$
                                </span>
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/trip4.jpg" alt="">
                                <div class="content text-center">
                                    <div class="row margin-leftright-null">
                                        <div class="meta">
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3>8</h3>
                                                <h4>Days</h4>
                                            </div>
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3>5</h3>
                                                <h4>Max Group Size</h4>
                                            </div>
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3>Easy</h3>
                                                <h4>Difficolty</h4>
                                            </div>
                                        </div>
                                        <div class="category">
                                            <h3>Trekking Expedition</h3>
                                        </div>
                                        <div class="info">
                                            <div class="col-md-12">
                                                <h6>Eolie Island</h6>
                                                <p class="margin-null">Eolie Island, Italy</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="single-trek-2.html" class="link"></a>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <!--  END Trip Showcase  -->
        <!--  Section Image Background with overlay  -->
        <div class="row margin-leftright-null grey-background">
            <div class="bg-img overlay simple-parallax responsive" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/img/testimonials.jpg)">
                <div class="container">
                    <!-- Testimonials -->
                    <section class="testimonials-carousel-simple col-md-12 text padding-bottom-null">
                        <div class="item padding-leftright-null">
                            <div class="padding-top-null padding-bottom-null">
                                <blockquote class="margin-bottom-small white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur voluptatum fugiat molestias, veritatis perspiciatis laborum modi beatae placeat explicabo at laudantium aliquam, nam vero ut!<em class="small grey-light">John Doe</em></blockquote>
                            </div>
                        </div>
                        <div class="item padding-leftright-null">
                            <div class="padding-top-null padding-bottom-null">
                                <blockquote class="margin-bottom-small white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur voluptatum fugiat molestias, veritatis perspiciatis laborum modi beatae placeat explicabo at laudantium aliquam, nam vero ut!<em class="small grey-light">Vanessa Brown</em></blockquote>
                            </div>
                        </div>
                    </section>
                    <!-- END Testimonials -->
                </div>
            </div>
        </div>
        <!--  END Section Image Background with overlay  -->
        <!--  Team  -->
        <div id="team">
            <div class="container text">
                <div class="row">
                    <div class="col-md-12 padding-leftright-null padding-onlybottom-md text-center">
                        <h2 class="margin-bottom-null title line center">Our fabulous team</h2>
                        <p class="heading center grey margin-bottom-null">Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                    </div>
                    <div class="col-md-4 single-person">
                        <div class="content">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/img/hiker1.jpg" alt="">
                            <ul class="social">
                                <li><a href="#"><i class="icon ion-social-twitter"></i></a></li>
                                <li><a href="#"><i class="icon ion-social-dribbble"></i></a></li>
                                <li><a href="#"><i class="icon ion-social-instagram"></i></a></li>
                                <li><a href="#"><i class="icon ion-social-snapchat"></i></a></li>
                            </ul>
                        </div>
                        <div class="description text-center">
                            <h5>John Doe</h5>
                            <h6>Cortina, Italy</h6>
                        </div>
                    </div>
                    <div class="col-md-4 single-person">
                        <div class="content">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/img/hiker2.jpg" alt="">
                            <ul class="social">
                                <li><a href="#"><i class="icon ion-social-twitter"></i></a></li>
                                <li><a href="#"><i class="icon ion-social-dribbble"></i></a></li>
                                <li><a href="#"><i class="icon ion-social-instagram"></i></a></li>
                                <li><a href="#"><i class="icon ion-social-snapchat"></i></a></li>
                            </ul>
                        </div>
                        <div class="description text-center">
                            <h5>Jessica Simpson</h5>
                            <h6>Chamonix-Mont-Blanc, France</h6>
                        </div>
                    </div>
                    <div class="col-md-4 single-person">
                        <div class="content">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/img/hiker3.jpg" alt="">
                            <ul class="social">
                                <li><a href="#"><i class="icon ion-social-twitter"></i></a></li>
                                <li><a href="#"><i class="icon ion-social-dribbble"></i></a></li>
                                <li><a href="#"><i class="icon ion-social-instagram"></i></a></li>
                                <li><a href="#"><i class="icon ion-social-snapchat"></i></a></li>
                            </ul>
                        </div>
                        <div class="description text-center">
                            <h5>Robert Brown</h5>
                            <h6>Kitzbühel, Austria</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  END Team  -->
        <!-- Section News -->
        <div class="row margin-leftright-null grey-background">
            <div class="container">
                <div class="col-md-12 padding-leftright-null text padding-bottom-null text-center">
                    <h2 class="margin-bottom-null title line center">Latest of our blog</h2>
                    <p class="heading center grey margin-bottom-null">Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                </div>
                <div class="col-md-12 text" id="news">
                    <!-- Single News -->
                    <div class="col-sm-6 single-news horizontal-news">
                        <article>
                            <div class="col-md-6 padding-leftright-null">
                                <div class="image" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/img/news1.jpg)"></div>
                            </div>
                            <div class="col-md-6 padding-leftright-null">
                                <div class="content">
                                    <h3>Adventure Time</h3>
                                    <span class="date">02.11.2016</span>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita, voluptas corporis. Maxime sapiente, adipisci laborum.</p>
                                    <span class="category">Travel</span>
                                    <span class="category">Blog</span>
                                </div>
                            </div>
                            <a href="standard-post.html" class="link"></a>
                        </article>
                    </div>
                    <!-- END Single News -->
                    <div class="col-sm-6 single-news horizontal-news">
                        <article>
                            <div class="col-md-6 padding-leftright-null">
                                <div class="image" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/img/news2.jpg)"></div>
                            </div>
                            <div class="col-md-6 padding-leftright-null">
                                <div class="content">
                                    <h3>Wild Mountain</h3>
                                    <span class="date">05.07.2016</span>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita, voluptas corporis. Maxime sapiente, adipisci laborum.</p>
                                    <span class="category">Travel</span>
                                    <span class="category">Mountain</span>
                                </div>
                            </div>
                            <a href="video-post.html" class="link"></a>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Section News -->
        <!--  Sponsor  -->
        <div class="row no-margin">
            <div class="container text">
                <div class="col-md-12 sponsor-carousel padding-leftright-null">
                    <div class="item">
                        <img class="center" src="<?php echo get_template_directory_uri() ?>/assets/img/sponsor1.png" alt="client logo">
                    </div>
                    <div class="item">
                        <img class="center" src="<?php echo get_template_directory_uri() ?>/assets/img/sponsor2.png" alt="client logo">
                    </div>
                    <div class="item">
                        <img class="center" src="<?php echo get_template_directory_uri() ?>/assets/img/sponsor3.png" alt="client logo">
                    </div>
                    <div class="item">
                        <img class="center" src="<?php echo get_template_directory_uri() ?>/assets/img/sponsor4.png" alt="client logo">
                    </div>
                </div>
            </div>
        </div>
        <!--  Sponsor  -->
    </div>
</div>
<!--  END Page Content, class footer-fixed if footer is fixed  -->
<?php get_footer(); ?>