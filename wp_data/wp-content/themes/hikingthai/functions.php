<?php
/**
 * hikingthai functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package hikingthai
 */

if ( ! function_exists( 'hikingthai_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function hikingthai_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on hikingthai, use a find and replace
		 * to change 'hikingthai' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'hikingthai', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'hikingthai' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'hikingthai_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'hikingthai_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function hikingthai_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'hikingthai_content_width', 640 );
}
add_action( 'after_setup_theme', 'hikingthai_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function hikingthai_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'hikingthai' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'hikingthai' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'hikingthai_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function hikingthai_scripts() {
	wp_enqueue_style( 'hikingthai-style', get_stylesheet_uri() );	

	wp_enqueue_style( 'hikingthai-style-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap/bootstrap.min.css', array(), time(), 'all' );
	wp_enqueue_style( 'hikingthai-style-bootstrap-theme', get_template_directory_uri() . '/assets/css/bootstrap/bootstrap-theme.min.css', array(), time(), 'all' );
	wp_enqueue_style( 'hikingthai-style-style', get_template_directory_uri() . '/assets/css/style.css', array(), time(), 'all' );
	wp_enqueue_style( 'hikingthai-style-font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array(), time(), 'all' );
	wp_enqueue_style( 'hikingthai-style-ionicons', get_template_directory_uri() . '/assets/css/ionicons.min.css', array(), time(), 'all' );
	wp_enqueue_style( 'hikingthai-style-puredesign', get_template_directory_uri() . '/assets/css/puredesign.css', array(), time(), 'all' );
	wp_enqueue_style( 'hikingthai-style-flexslider', get_template_directory_uri() . '/assets/css/flexslider.css', array(), time(), 'all' );
	wp_enqueue_style( 'hikingthai-style-owlcarousel', get_template_directory_uri() . '/assets/css/owl.carousel.css', array(), time(), 'all' );
	wp_enqueue_style( 'hikingthai-style-magnific', get_template_directory_uri() . '/assets/css/magnific-popup.css', array(), time(), 'all' );
	wp_enqueue_style( 'hikingthai-style-fullPage', get_template_directory_uri() . '/assets/css/jquery.fullPage.css', array(), time(), 'all' );

	wp_enqueue_script( 'hikingthai-script-jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), time(), false );
	wp_enqueue_script( 'hikingthai-script-flexslider', get_template_directory_uri() . '/assets/js/jquery.flexslider-min.js', array(), time(), true );
	wp_enqueue_script( 'hikingthai-script-fullPage', get_template_directory_uri() . '/assets/js/jquery.fullPage.min.js', array(), time(), true );
	wp_enqueue_script( 'hikingthai-script-owlcarousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array(), time(), true );
	wp_enqueue_script( 'hikingthai-script-isotope', get_template_directory_uri() . '/assets/js/isotope.min.js', array(), time(), true );
	wp_enqueue_script( 'hikingthai-script-magnific-popup', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js', array(), time(), true );
	wp_enqueue_script( 'hikingthai-script-googleapis', 'https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true', array(), time(), true );
	wp_enqueue_script( 'hikingthai-script-scrollTo', get_template_directory_uri() . '/assets/js/jquery.scrollTo.min.js', array(), time(), true );
	wp_enqueue_script( 'hikingthai-script-appear', get_template_directory_uri() . '/assets/js/jquery.appear.js', array(), time(), true );
	wp_enqueue_script( 'hikingthai-script-countTo', get_template_directory_uri() . '/assets/js/jquery.countTo.js', array(), time(), true );
	wp_enqueue_script( 'hikingthai-script-scrolly', get_template_directory_uri() . '/assets/js/jquery.scrolly.js', array(), time(), true );
	wp_enqueue_script( 'hikingthai-script-imagesloaded', get_template_directory_uri() . '/assets/js/imagesloaded.min.js', array(), time(), true );
	wp_enqueue_script( 'hikingthai-script-pace', get_template_directory_uri() . '/assets/js/pace.min.js', array(), time(), true );
	wp_enqueue_script( 'hikingthai-script-main', get_template_directory_uri() . '/assets/js/main.js', array(), time(), true );
	
}
add_action( 'wp_enqueue_scripts', 'hikingthai_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

